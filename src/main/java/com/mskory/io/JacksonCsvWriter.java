package com.mskory.io;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mskory.model.Pojo;
import com.mskory.service.PojoValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class JacksonCsvWriter implements DataWriter {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String DEFAULT_VALID_FILE_NAME = "valid_messages.csv";
    private static final String DEFAULT_INVALID_FILE_NAME = "invalid_messages.csv";
    private final Queue<Pojo> validPojos;
    private final Queue<Pojo> invalidPojos;
    private final AtomicBoolean validatorState;
    private List<String> validHeaders;
    private List<String> invalidHeaders;
    private boolean writeValid;
    private boolean writeInvalid;
    private String validFileName;
    private String invalidFileName;
    private int messagesWrote;


    public JacksonCsvWriter(PojoValidator validator) {
        this.validatorState = validator.isAlive();
        this.validPojos = validator.getValid();
        this.invalidPojos = validator.getInvalid();
        validHeaders = Collections.emptyList();
        invalidHeaders = Collections.emptyList();
        validFileName = DEFAULT_VALID_FILE_NAME;
        invalidFileName = DEFAULT_INVALID_FILE_NAME;
    }

    @Override
    public void write() {
        if (writeValid) {
            logger.info("Starting to write valid messages");
            Thread thread = new Thread(this::writeValid);
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        if (writeInvalid) {
            logger.info("Starting to write invalid messages");
            Thread thread = new Thread(this::writeInvalid);
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void writeInvalid() {
        while (invalidPojos.isEmpty()) Thread.onSpinWait();
        invalidHeaders = setHeaders(invalidPojos.peek(), invalidHeaders);
        invalidHeaders.add("error");
        write(invalidHeaders, invalidFileName, invalidPojos);
        logger.info("Invalid messages were wrote to file {}", invalidFileName);
    }

    private void writeValid() {
        while (validPojos.isEmpty()) Thread.onSpinWait();
        validHeaders = setHeaders(validPojos.peek(), validHeaders);
        write(validHeaders, validFileName, validPojos);
        logger.info("Valid messages were wrote to file {}", validFileName);
    }

    private void write(List<String> headers, String fileName, Queue<Pojo> pojos) {
        ObjectWriter writer = getObjectWriter(headers);
        File outputFile = new File(fileName);
        try (SequenceWriter sequenceWriter = writer.writeValues(outputFile)) {
            while (validatorState.get() || !pojos.isEmpty()) {
                Pojo pojo = pojos.poll();
                if (pojo == null) continue;
                sequenceWriter.write(pojo);
                messagesWrote++;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ObjectWriter getObjectWriter(List<String> headers) {
        CsvMapper mapper = new CsvMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);
        CsvSchema schema = CsvSchema.builder()
                .addColumns(headers, CsvSchema.ColumnType.STRING_OR_LITERAL)
                .build()
                .withHeader();
        return mapper.writer().with(schema);
    }

    private List<String> setHeaders(Pojo pojo, List<String> headers) {
        List<Field> fieldsToWrite = getPojoFields(pojo);
        if (headers.isEmpty()) {
            return fieldsToWrite.stream()
                    .map(Field::getName)
                    .collect(Collectors.toList());
        }
        return headers;
    }

    private List<Field> getPojoFields(Pojo pojo) {
        return List.of(pojo.getClass().getDeclaredFields());
    }

    public JacksonCsvWriter setWriteValid(boolean writeValid) {
        this.writeValid = writeValid;
        return this;
    }

    public JacksonCsvWriter setWriteInvalid(boolean writeInvalid) {
        this.writeInvalid = writeInvalid;
        return this;
    }

    public JacksonCsvWriter setValidHeaders(List<String> validHeaders) {
        this.validHeaders = validHeaders;
        return this;
    }

    public JacksonCsvWriter setInvalidHeaders(List<String> invalidHeaders) {
        this.invalidHeaders = invalidHeaders;
        return this;
    }

    public JacksonCsvWriter setValidFileName(String validFileName) {
        this.validFileName = validFileName;
        return this;
    }

    public JacksonCsvWriter setInvalidFileName(String invalidFileName) {
        this.invalidFileName = invalidFileName;
        return this;
    }

    public int getMessagesWrote() {
        return messagesWrote;
    }
}

