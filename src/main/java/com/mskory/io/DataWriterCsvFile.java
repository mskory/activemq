//package com.mskory.io;
//
//import com.mskory.model.Pojo;
//import com.opencsv.CSVWriter;
//import jakarta.validation.ConstraintViolation;
//import jakarta.validation.Validation;
//import jakarta.validation.Validator;
//import jakarta.validation.ValidatorFactory;
//import org.json.JSONObject;
//
//import java.io.FileWriter;
//import java.io.IOException;
//import java.lang.reflect.Field;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//import java.util.stream.Collectors;
//
//public class DataWriterCsvFile implements DataWriter {
//
//    private static final String DEFAULT_FILE_NAME = "data.csv";
//    private List<? extends Pojo> pojos;
//    private String fileName;
//    private boolean writeErrors;
//    private List<String> headers;
//    private List<Field> fieldsToWrite;
//    private Validator validator;
//
//    public DataWriterCsvFile() {
//        pojos = Collections.emptyList();
//        fileName = DEFAULT_FILE_NAME;
//        writeErrors = false;
//        headers = Collections.emptyList();
//    }
//
//    private void prepare() {
//        fieldsToWrite = getPojoFields(pojos.get(0));
//        if (headers.isEmpty()) {
//            setHeadersFromPojoFields();
//        } else {
//            setFieldsRelativelyHeaders();
//        }
//        if (writeErrors){
//            headers.add("error");
//        }
//    }
//
//    private void setFieldsRelativelyHeaders() {
//        fieldsToWrite = fieldsToWrite.stream()
//                .filter(f -> headers.contains(f.getName()))
//                .sorted(getComparator())
//                .collect(Collectors.toList());
//    }
//
//    private Comparator<? super Field> getComparator() {
//        return (Comparator<Field>) (o1, o2) -> Integer.compare(
//                headers.indexOf(o1.getName()), headers.indexOf(o2.getName()));
//    }
//
//    private List<String> getFieldsValues(Pojo pojo) {
//        List<String> values = fieldsToWrite.stream()
//                .map(field -> {
//                    field.setAccessible(true);
//                    try {
//                        String value = field.get(pojo).toString();
//                        field.setAccessible(false);
//                        return value;
//                    } catch (IllegalAccessException e) {
//                        throw new RuntimeException(e);
//                    }
//                })
//                .collect(Collectors.toList());
//        if (writeErrors) {
//            values.add(getErrors(pojo));
//        }
//        return values;
//    }
//
//    private List<Field> getPojoFields(Pojo pojo) {
//        List<Field> fields = new ArrayList<>();
//        Class<?> tmp = pojo.getClass();
//        while (tmp.getSuperclass() != null) {
//            fields.addAll(List.of(tmp.getDeclaredFields()));
//            tmp = tmp.getSuperclass();
//        }
//        return List.of(pojo.getClass().getDeclaredFields());
//    }
//
//    private String getErrors(Pojo pojo) {
//        String message = validator.validate(pojo)
//                .stream()
//                .map(ConstraintViolation::getMessage)
//                .collect(Collectors.joining(", "));
//        return new JSONObject().put("error", message).toString();
//    }
//
//    private void setHeadersFromPojoFields() {
//        headers = fieldsToWrite.stream()
//                .map(Field::getName)
//                .collect(Collectors.toList());
//    }
//
//
//    public void write() {
//        prepare();
//        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
//        validator = factory.getValidator();
//        try (FileWriter outputFile = new FileWriter(fileName); CSVWriter writer = new CSVWriter(outputFile)) {
//            writer.writeNext(headers.toArray(new String[0]));
//            pojos.stream()
//                    .map(this::getFieldsValues)
//                    .forEach(l -> writer.writeNext(l.toArray(new String[0])));
//            factory.close();
//        } catch (IOException e) {
//            throw new RuntimeException("Can't write data to file" + fileName, e);
//        }
//    }
//
//    public DataWriterCsvFile setPojos(List<? extends Pojo> pojos) {
//        this.pojos = pojos;
//        return this;
//    }
//
//    public DataWriterCsvFile setFileName(String fileName) {
//        this.fileName = fileName;
//        return this;
//    }
//
//    public DataWriterCsvFile setWriteErrors(boolean writeErrors) {
//        this.writeErrors = writeErrors;
//        return this;
//    }
//
//    public DataWriterCsvFile setHeaders(List<String> headers) {
//        this.headers = headers;
//        return this;
//    }
//
//}
