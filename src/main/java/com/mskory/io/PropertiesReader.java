package com.mskory.io;

import com.mskory.exceptions.PropertiesNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Properties;


public class PropertiesReader {
    private static final Logger logger = LoggerFactory.getLogger(PropertiesReader.class);
    private final String fileName;

    public PropertiesReader(String fileName) {
        this.fileName = fileName;
    }

    public Properties getProperties() {
        logger.info("Reading a properties file");
        Properties properties = new Properties();
        try (InputStream stream = getClass().getClassLoader().getResourceAsStream(fileName)) {
            if (stream == null){
                return properties;
            }
            InputStreamReader reader = new InputStreamReader(stream);
            properties.load(reader);
            reader.close();
            return properties;
        } catch (Exception e) {
            throw new PropertiesNotFoundException("Can't read file " + fileName + " in config directory.");
        }
    }
}
