package com.mskory.model;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.time.LocalDate;
import java.util.Objects;

public class PojoMessage extends Pojo {
    private LocalDate createdAt;
    private int count;
    private String name;
    public PojoMessage() {
    }

    public PojoMessage(String name, int count, LocalDate createdAt) {
        this.name = name;
        this.count = count;
        this.createdAt = createdAt;
    }

    @Size(min = 7)
    @Pattern(regexp = "\\w*[aA]+\\w*")
    public String getName() {
        return name;
    }

    @Min(value = 10)
    public int getCount() {
        return count;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PojoMessage message = (PojoMessage) o;
        return count == message.count && createdAt.equals(message.createdAt) && name.equals(message.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(createdAt, count, name);
    }
}
