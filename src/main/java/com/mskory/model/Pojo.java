package com.mskory.model;

import java.io.Serializable;

public class Pojo implements Serializable {
    private String error;
    private boolean isPoisonPill;
    private boolean isStartMessage;

    public Pojo setPoisonPill(boolean poisonPill) {
        this.isPoisonPill = poisonPill;
        return this;
    }

    public boolean isPoisonPill() {
        return isPoisonPill;
    }

    public String getError() {
        return error;
    }

    public Pojo setError(String error) {
        this.error = error;
        return this;
    }

    public Pojo setStartMessage(boolean startMessage) {
        this.isStartMessage = startMessage;
        return this;
    }

    public boolean isStartMessage() {
        return isStartMessage;
    }
}
