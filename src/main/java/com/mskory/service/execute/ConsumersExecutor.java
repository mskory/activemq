package com.mskory.service.execute;

import com.mskory.model.Pojo;
import com.mskory.service.Configuration;
import com.mskory.service.Consumer;
import com.mskory.service.ResultsProcessor;
import com.mskory.service.supply.SessionSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

public class ConsumersExecutor implements Runnable{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ResultsProcessor results = new ResultsProcessor();
    private final AtomicBoolean executorState = new AtomicBoolean();
    private final SessionSupplier sessionSupplier;
    private final Queue<Pojo> messages;
    private final Configuration config;
    private long consumersCount;

    public ConsumersExecutor(Configuration config, SessionSupplier sessionSupplier, Queue<Pojo> messages) {
        this.config = config;
        this.sessionSupplier = sessionSupplier;
        this.messages = messages;
    }

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        executorState.set(true);
        ExecutorService consumerExecutor = Executors.newFixedThreadPool(config.getConsumersNumber());
        consumersCount = Stream.generate(() -> new Consumer(config, sessionSupplier, messages, results))
                .limit(config.getConsumersNumber())
                .peek(consumerExecutor::execute)
                .count();
        consumerExecutor.shutdown();
        while (!consumerExecutor.isTerminated()) {
            Thread.onSpinWait();
        }
        executorState.set(false);
        logger.info("Consumers were stopped");
        logger.info(getResults());
        sessionSupplier.closeConnections();
    }

    public long getConsumersCount() {
        return consumersCount;
    }

    public String getResults() {
        return String.format(" Results for Consumers(s) %n Total Consumers %s%n Total messages processed %s%n " +
                        "Total RPS %.2f%n Average RPS per Consumer %.2f%n",
                consumersCount, results.getTotalMessagesProcessed(),
                results.getTotalRPS(), results.getAverageRPS());
    }

    public AtomicBoolean isAlive(){
        return executorState;
    }
}
