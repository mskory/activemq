package com.mskory.service.execute;

import com.mskory.model.Pojo;
import com.mskory.service.Configuration;
import com.mskory.service.PoisonPillProducer;
import com.mskory.service.Producer;
import com.mskory.service.ResultsProcessor;
import com.mskory.service.supply.RandomPojoSupplier;
import com.mskory.service.supply.SessionSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

public class ProducersExecutor implements Runnable {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    private final RandomPojoSupplier<? extends Pojo> randomPojoSupplier;
    private final SessionSupplier sessionSupplier;
    private final Configuration config;
    private final ResultsProcessor results = new ResultsProcessor();
    private final ExecutorService executorService;
    private PoisonPillProducer poisonPillProducer;
    private int producersNumber;
    private long producersCount;

    public ProducersExecutor(Configuration config, RandomPojoSupplier<? extends Pojo> randomPojoSupplier, SessionSupplier sessionSupplier) {
        this.producersNumber = config.getProducersNumber();
        this.config = config;
        this.randomPojoSupplier = randomPojoSupplier;
        this.sessionSupplier = sessionSupplier;
        this.poisonPillProducer = new PoisonPillProducer(config.getQueueName(), sessionSupplier);
        this.executorService = Executors.newFixedThreadPool(producersNumber);
    }

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        producersCount = Stream.generate(() -> new Producer(config, sessionSupplier, randomPojoSupplier, results))
                .limit(producersNumber - 1)
                .peek(executorService::execute)
                .count();
        executorService.execute(new Producer(config, sessionSupplier, randomPojoSupplier, results)
                .setMessagesAmount(config.getMessagesAmountFinalProducer()));
        executorService.shutdown();
        while (!executorService.isTerminated()) {
            Thread.onSpinWait();
        }
        logger.info("All Producers were sopped");
        poisonPillProducer.send();
        logger.info(getResults());
    }

    public long getProducersCount() {
        return producersCount + 1;
    }

    public void setPoisonPillProducer(PoisonPillProducer poisonPillProducer) {
        this.poisonPillProducer = poisonPillProducer;
    }

    public boolean isAlive() {
        return !executorService.isTerminated();
    }

    public String getResults() {
        return String.format(" Results for Producer(s) %n Total Producers %s%n Total messages processed %s%n " +
                        "Total RPS %.2f%n Average RPS per Producer %.2f%n",
                producersCount + 1, results.getTotalMessagesProcessed(),
                results.getTotalRPS(), results.getAverageRPS());
    }
}
