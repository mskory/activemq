package com.mskory.service.supply;

import javax.jms.Connection;
import javax.jms.Session;

public interface SessionSupplier {
    Session createSession();
    Connection createConnection();

    void closeConnections();
}
