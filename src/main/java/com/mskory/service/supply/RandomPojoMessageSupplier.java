package com.mskory.service.supply;

import com.mskory.model.PojoMessage;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

public class RandomPojoMessageSupplier implements RandomPojoSupplier<PojoMessage> {
    private final int maxCount;
    private final int maxNameLength;

    public RandomPojoMessageSupplier(int maxNameLength, int maxCount) {
        this.maxNameLength =  maxNameLength;
        this.maxCount = maxCount;
    }

    @Override
    public PojoMessage generate() {
        int count = ThreadLocalRandom.current().nextInt(maxCount + 1);
        String name =  getRandomString(1, maxNameLength);
        LocalDate date = getRandomDate();
        return new PojoMessage(name, count, date);
    }


}
