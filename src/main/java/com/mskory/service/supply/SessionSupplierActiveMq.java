package com.mskory.service.supply;

import com.mskory.service.Configuration;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Session;

public class SessionSupplierActiveMq implements SessionSupplier {
    private PooledConnectionFactory  pooledConnectionFactory = new PooledConnectionFactory();
    private Connection defaultConnection;

    public SessionSupplierActiveMq(Configuration config) {
        ActiveMQConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory(config.getLogin(), config.getPasswd(), config.getUrl());
        pooledConnectionFactory.setConnectionFactory(connectionFactory);
        pooledConnectionFactory.setMaxConnections(config.getMaxConnections());
        try {
            defaultConnection = pooledConnectionFactory.createConnection();
            defaultConnection.start();
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Session createSession() {
        try {
            return defaultConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        } catch (Exception e) {
            throw new RuntimeException("Can't create a new session", e);
        }
    }

    @Override
    public Connection createConnection(){
        try {
            Connection connection = pooledConnectionFactory.createConnection();
            connection.start();
            return connection;
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void closeConnections() {
        try {
            pooledConnectionFactory.clear();
        } catch (Exception e) {
            throw new RuntimeException("Can't close connections", e);
        }
    }

    public SessionSupplierActiveMq setPooledConnectionFactory(PooledConnectionFactory pooledConnectionFactory) {
        this.pooledConnectionFactory = pooledConnectionFactory;
        return this;
    }

    public SessionSupplierActiveMq setDefaultConnection(Connection defaultConnection) {
        this.defaultConnection = defaultConnection;
        return this;
    }
}
