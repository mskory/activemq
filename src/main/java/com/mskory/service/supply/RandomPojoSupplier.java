package com.mskory.service.supply;

import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

public interface RandomPojoSupplier<T> {

    T generate();

    default String getRandomString(int minLength, int maxLength) {
        return RandomStringUtils.randomAlphabetic(ThreadLocalRandom.current()
                .nextInt(minLength, maxLength + 1));
    }

    default LocalDate getRandomDate() {
        long minDay = LocalDate.EPOCH.toEpochDay();
        long maxDay = LocalDate.now().toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
        return LocalDate.ofEpochDay(randomDay);
    }
}
