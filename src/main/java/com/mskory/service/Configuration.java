package com.mskory.service;

import com.mskory.io.PropertiesReader;

import java.util.Properties;

public class Configuration {
    public static final String PROPERTIES_NAME = "app.properties";
    public static final String LIFE_TIME_PROPERTIES_NAME = "producerLifeTime";
    public static final String BROKER_URL_PROPERTIES_NAME = "connection";
    public static final String QUEUE_PROPERTIES_NAME = "queueName";
    public static final String PASSWORD_FIELD_NAME = "password";
    public static final String LOGIN_FIELD_NAME = "login";
    public static final String NUMBER_OF_PRODUCERS_FIELD_NAME = "producers";
    public static final String NUMBER_OF_CONSUMERS_FIELD_NAME = "consumers";
    private final String password;
    private final String login;
    private final String brokerUrl;
    private final String queueName;
    private final long lifeTime;
    private final int maxConnections;
    private final int msgAmount;
    private int producersNumber;
    private int consumersNumber;

    public Configuration(String[] args) {
        PropertiesReader propertiesReader = new PropertiesReader(PROPERTIES_NAME);
        Properties properties = propertiesReader.getProperties();
        password = properties.getProperty(PASSWORD_FIELD_NAME);
        login = properties.getProperty(LOGIN_FIELD_NAME);
        brokerUrl = properties.getProperty(BROKER_URL_PROPERTIES_NAME);
        queueName = properties.getProperty(QUEUE_PROPERTIES_NAME);
        lifeTime = Integer.parseInt(properties.getProperty(LIFE_TIME_PROPERTIES_NAME)) * 1000L;
        producersNumber = Integer.parseInt(properties.getProperty(NUMBER_OF_PRODUCERS_FIELD_NAME));
        consumersNumber = Integer.parseInt(properties.getProperty(NUMBER_OF_CONSUMERS_FIELD_NAME));
        maxConnections = producersNumber + 1;
        msgAmount = getMessagesAmount(args);
    }

    private int getMessagesAmount(String[] args) {
        try {
            if (args.length == 0) throw new RuntimeException("Arguments not found.");
            int n = Integer.parseInt(args[0].split("=")[0].trim());
            if (n <= 0) throw new RuntimeException("The argument must be greater then 0.");
            return n;
        } catch (Exception e) {
            throw new IllegalArgumentException("Can't parse input arguments.", e);
        }
    }

    public int getMessagesPerProducer() {
        return msgAmount / producersNumber;
    }

    public int getMessagesAmountFinalProducer() {
        return getMessagesPerProducer() + (msgAmount % producersNumber);
    }

    public String getPasswd() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    public String getUrl() {
        return brokerUrl;
    }

    public String getQueueName() {
        return queueName;
    }

    public long getLifeTime() {
        return lifeTime;
    }

    public int getProducersNumber() {
        return producersNumber;
    }

    public int getConsumersNumber() {
        return consumersNumber;
    }

    public int getMaxConnections() {
        return maxConnections;
    }

    public int getMsgAmount() {
        return msgAmount;
    }

    public void setConsumersNumber(int consumersNumber) {
        this.consumersNumber = consumersNumber;
    }

    public void setProducersNumber(int producersNumber) {
        this.producersNumber = producersNumber;
    }
}
