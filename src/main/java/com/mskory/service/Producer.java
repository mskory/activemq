package com.mskory.service;

import com.mskory.model.Pojo;
import com.mskory.service.supply.RandomPojoSupplier;
import com.mskory.service.supply.SessionSupplier;

import org.apache.activemq.util.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import java.util.stream.Stream;

public class Producer implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final RandomPojoSupplier<? extends Pojo> pojoSupplier;
    final SessionSupplier sessionSupplier;
    private final String queueName;
    private final long lifeTime;
    private int messagesAmount;
    private MessageProducer queueProducer;
    private Session session;
    private Connection connection;
    private int processedMessages;
    private ResultsProcessor results;

    public Producer(Configuration config, SessionSupplier sessionSupplier,
                    RandomPojoSupplier<? extends Pojo> pojoSupplier, ResultsProcessor results) {
        this.results = results;
        this.messagesAmount = config.getMessagesPerProducer();
        this.queueName = config.getQueueName();
        this.lifeTime = config.getLifeTime();
        this.sessionSupplier = sessionSupplier;
        this.pojoSupplier = pojoSupplier;
    }

    public void run() {
        try {
            logger.info("Create the destination Queue");
            connection = sessionSupplier.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//            session = sessionSupplier.createSession();
            Destination queueDestination = session.createQueue(queueName);
            queueProducer = session.createProducer(queueDestination);
            queueProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            StopWatch stopWatch = new StopWatch(true);
            processedMessages = (int) Stream.generate(this::createMessage)
                    .takeWhile(p -> lifeTime > stopWatch.taken())
                    .limit(messagesAmount)
                    .peek(this::sendMessage)
                    .count();
            session.close();
            connection.close();
            results.addResults(stopWatch.taken(), processedMessages);
//            printResults(stopWatch.taken(), processedMessages);
        } catch (Exception e) {
            throw new RuntimeException("Can't send message", e);
        }
    }

    public boolean start() {
        Thread thread = new Thread(this);
        thread.start();
        return true;
    }

    private boolean sendMessage(ObjectMessage objectMessage) {
        try {
            queueProducer.send(objectMessage);
            return true;
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    private ObjectMessage createMessage() {
        ObjectMessage msg;
        try {
            msg = session.createObjectMessage(pojoSupplier.generate());
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
        return msg;
    }

    private void printResults(long workingTime, int messagesAmount) {
        double workingTimeInSeconds = workingTime / 1000d;
        logger.info("\n Working time {}\n Messages processed {}\n Request per second {}",
                workingTimeInSeconds, messagesAmount, messagesAmount / workingTimeInSeconds);
    }

    public int getProcessedMessages() {
        return processedMessages;
    }

    public Producer setMessagesAmount(int messagesAmount) {
        this.messagesAmount = messagesAmount;
        return this;
    }
}
