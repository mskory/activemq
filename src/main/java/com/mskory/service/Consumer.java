package com.mskory.service;

import com.mskory.model.Pojo;
import com.mskory.service.supply.SessionSupplier;
import org.apache.activemq.util.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;

import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.util.Queue;

public class Consumer implements Runnable {
    private static final String NAME = "Consumer";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Queue<Pojo> messages;
    private final SessionSupplier sessionSupplier;
    private final String queueName;
    private ResultsProcessor results;
    private StopWatch stopWatch;
    private Connection connection;
    private Session session;
    private int messagesCount;

    public Consumer(Configuration config, SessionSupplier sessionSupplier, Queue<Pojo> messages, ResultsProcessor results) {
        this.results = results;
        this.messages = messages;
        this.queueName = config.getQueueName();
        this.sessionSupplier = sessionSupplier;
    }

    public void run() {
        logger.info("Consumer started");
        try {
//            connection = sessionSupplier.createConnection();
//            connection.start();
//            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            session = sessionSupplier.createSession();
            Destination destinationQueue = session.createQueue(queueName);
            MessageConsumer queueConsumer = session.createConsumer(destinationQueue);
            stopWatch = new StopWatch(true);
            listenQueue(queueConsumer);
        } catch (Exception e) {
            throw new RuntimeException("Can't receive a message", e);
        }
    }

    private void listenQueue(MessageConsumer queueConsumer) {
        Message message;
        while (true) {
            try {
                message = queueConsumer.receive();
                if (message instanceof TextMessage) {
                    isPoisonPill(message);
                    break;
                } else {
                    Pojo pojo = toPojo(message);
                    messages.add(pojo);
                    messagesCount++;
                }
            } catch (JMSException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private Pojo toPojo(Message message) {
        try {
            ObjectMessage objMsg = (ObjectMessage) message;
            return (Pojo) objMsg.getObject();
        } catch (Exception e) {
            throw new RuntimeException("Can't cast received object to pojo", e);
        }
    }

    private void isPoisonPill(Message msg) {
        try {
            if (((TextMessage) msg).getText().equals("STOP")) {
                results.addResults(stopWatch.taken(), messagesCount);
                //  printResults(stopWatch.taken());
                logger.info("PoisonPill was received");
                close();
            }
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    private void printResults(long workingTime) {
        double timeInSeconds = workingTime / 1000d;
        logger.info("\n Working time {}\n Messages processed {}\n Request per second {}",
                timeInSeconds, messagesCount, messagesCount / timeInSeconds);
    }

    private void close() {
        try {
            session.close();
//            connection.close();
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    public void start() {
        Thread thread = new Thread(this, NAME);
        thread.start();
    }

    public long getMessagesCount() {
        return messagesCount;
    }

    public Queue<Pojo> getMessages() {
        return messages;
    }
}
