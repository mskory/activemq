package com.mskory.service;

import com.mskory.model.Pojo;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class PojoValidator implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Queue<Pojo> valid = new ConcurrentLinkedQueue<>();
    private final Queue<Pojo> invalid = new ConcurrentLinkedQueue<>();
    private final AtomicBoolean validatorState;
    private final AtomicBoolean externalConsumerState;
    private final Validator validator;
    private final Queue<Pojo> queue;
    private int validCount;
    private int invalidCount;

    public PojoValidator(Locale msgLanguage, Queue<Pojo> queue, AtomicBoolean externalConsumerState) {
        validatorState = new AtomicBoolean();
        Locale.setDefault(msgLanguage);
        this.externalConsumerState = externalConsumerState;
        this.queue = queue;
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            validator = factory.getValidator();
        } catch (Exception e) {
            throw new RuntimeException("Can't create validator", e);
        }
    }

    @Override
    public void run() {
        validatorState.set(true);
        logger.info("Validator started");
        while (externalConsumerState.get() || !queue.isEmpty()) {
            Pojo pojo = queue.poll();
            if (pojo == null) {
                continue;
            }
            Set<ConstraintViolation<Pojo>> validate = validator.validate(pojo);
            if (validate.isEmpty()) {
                valid.add(pojo);
                validCount++;
            } else {
                pojo.setError(getErrors(pojo));
                invalid.add(pojo);
                invalidCount++;
            }
        }
        validatorState.set(false);
        logger.info("Validation complete. Messages processed {}", validCount + invalidCount);
        logger.info("There are {} valid and {} invalid pojos", validCount, invalidCount);
    }

    private String getErrors(Pojo pojo) {
        String message = validator.validate(pojo)
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.joining(", "));
        return new JSONObject().put("error", message).toString();
    }

    public void start() {
        Thread thr = new Thread(this, "Validator");
        thr.start();
    }

    public Queue<Pojo> getValid() {
        return valid;
    }

    public Queue<Pojo> getInvalid() {
        return invalid;
    }

    public AtomicBoolean isAlive() {
        return validatorState;
    }
}
