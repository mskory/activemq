package com.mskory.service;

import com.mskory.service.supply.SessionSupplier;
import org.apache.activemq.advisory.AdvisorySupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

public class PoisonPillProducer {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    final SessionSupplier sessionSupplier;
    private final String queueName;
    private Session session;

    public PoisonPillProducer(String queueName, SessionSupplier sessionSupplier) {
        this.sessionSupplier = sessionSupplier;
        this.queueName = queueName;
    }

    public boolean send() {
        logger.info("Poison pill Producer starts");
        try {
            session = sessionSupplier.createSession();
            Destination queueDestination = session.createQueue(queueName);
            Destination advisoryDestination = AdvisorySupport.getConsumerAdvisoryTopic(queueDestination);
            MessageConsumer advisoryConsumer = session.createConsumer(advisoryDestination);
            MessageProducer poisonPillProducer = session.createProducer(queueDestination);
            sendPoisonPill(poisonPillProducer, getConsumersNumber(advisoryConsumer));
            session.close();
            return true;
        } catch (Exception e) {
            throw new RuntimeException("Can't send message", e);
        }
    }

    private int getConsumersNumber(MessageConsumer advisoryConsumer) {
        try {
            Message advisoryMessage = advisoryConsumer.receive();
            return advisoryMessage.getIntProperty("consumerCount");
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    private void sendPoisonPill(MessageProducer messageProducer, int count) {
        try {
            System.out.println(count);
            for (int i = 0; i < count; i++) {
                messageProducer.send(session.createTextMessage("STOP"));
                logger.info("Poison pill sent");
            }
            logger.info("{} poison pill(s) sent", count);
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }
}
