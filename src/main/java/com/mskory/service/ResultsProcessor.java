package com.mskory.service;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class ResultsProcessor {

    private final AtomicLong totalWorkingTime = new AtomicLong();
    private final AtomicInteger totalMessagesProcessed = new AtomicInteger();
    private double totalRPS;

    public void addResults(long workingTime, int messagesCount) {
        totalWorkingTime.addAndGet(workingTime);
        totalMessagesProcessed.addAndGet(messagesCount);
        totalRPS += getAverageRPS();
    }

    public double getAverageRPS(){
        return totalMessagesProcessed.get() / (totalWorkingTime.get() / 1000d) ;
    }
    public double getTotalRPS(){
        return totalRPS;
    }
    public int getTotalMessagesProcessed(){
        return totalMessagesProcessed.get();
    }

}
