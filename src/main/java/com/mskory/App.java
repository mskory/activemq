package com.mskory;

import com.mskory.io.JacksonCsvWriter;
import com.mskory.model.Pojo;
import com.mskory.model.PojoMessage;
import com.mskory.service.Configuration;
import com.mskory.service.execute.ConsumersExecutor;
import com.mskory.service.PojoValidator;
import com.mskory.service.execute.ProducersExecutor;
import com.mskory.service.supply.SessionSupplier;
import com.mskory.service.supply.SessionSupplierActiveMq;
import com.mskory.service.supply.RandomPojoMessageSupplier;
import com.mskory.service.supply.RandomPojoSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Locale;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;


public class App {
    public static final int MAX_NAME_LENGTH = 50;
    public static final int MAX_COUNT = 100;
    public static final List<String> HEADERS = List.of("name", "count");

    public static void main(String[] args){
        Queue<Pojo> messages = new ConcurrentLinkedQueue<>();

        Configuration config = new Configuration(args);
        RandomPojoSupplier<PojoMessage> randomPojoSupplier = new RandomPojoMessageSupplier(MAX_NAME_LENGTH, MAX_COUNT);
        SessionSupplier sessionSupplier = new SessionSupplierActiveMq(config);

        ProducersExecutor producersExecutor = new ProducersExecutor(config,randomPojoSupplier, sessionSupplier);
        producersExecutor.start();

        ConsumersExecutor consumersExecutor = new ConsumersExecutor(config, sessionSupplier, messages);
        consumersExecutor.start();

//        PojoValidator validator = new PojoValidator(Locale.ENGLISH, messages, consumersExecutor.isAlive());
//        validator.start();
//
//        JacksonCsvWriter csvWriter = new JacksonCsvWriter(validator)
//                .setValidHeaders(HEADERS)
//                .setWriteValid(true)
//                .setWriteInvalid(true);
//        csvWriter.write();

    }
}
