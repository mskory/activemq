package com.mskory.service.supply;

import com.mskory.service.Configuration;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Session;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SessionSupplierActiveMqTest {
    @Mock
    PooledConnectionFactory pooledConnectionFactory;
    @Mock
    Connection defaultConnection;
    @Mock
    Session session;
    @Mock
    Connection connection;
    Configuration config = new Configuration(new String[]{String.valueOf(10)});
    SessionSupplierActiveMq sessionSupplier = new SessionSupplierActiveMq(config);

    //BeforeAll doesn't work :( null pointer exception
    @BeforeEach
    void setUp() throws JMSException {
        sessionSupplier.setDefaultConnection(defaultConnection);
        sessionSupplier.setPooledConnectionFactory(pooledConnectionFactory);
        lenient().when(defaultConnection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenReturn(session);
        lenient().when(pooledConnectionFactory.createConnection()).thenReturn(connection);
    }

    @Test
    void createSession_Ok() {
        assertEquals(session, sessionSupplier.createSession());
    }

    @Test
    void createConnection_Ok() {
        assertEquals(connection, sessionSupplier.createConnection());
    }

    @Test
    void closeConnections_Ok() {
        sessionSupplier.closeConnections();
        verify(pooledConnectionFactory).clear();
    }
}