package com.mskory.service;

import com.mskory.model.Pojo;
import com.mskory.model.PojoMessage;
import com.mskory.service.supply.RandomPojoMessageSupplier;
import com.mskory.service.supply.RandomPojoSupplier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class PojoValidatorTest {

    RandomPojoSupplier<PojoMessage> pojoSupplier = new RandomPojoMessageSupplier(100, 100);
    Queue<Pojo> messages;
    int messagesAmount;

    @BeforeEach
    void setUp() {
        messagesAmount = ThreadLocalRandom.current().nextInt(10, 100);
        messages = Stream.generate(() -> pojoSupplier.generate())
                .limit(messagesAmount)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Test
    void start_Ok() {
        AtomicBoolean consumerState = new AtomicBoolean(false);
        PojoValidator validator = new PojoValidator(Locale.ENGLISH, messages, consumerState);
        validator.run();
        System.out.println();
        assertEquals((int) ReflectionTestUtils.getField(validator, "validCount") +(int) ReflectionTestUtils.getField(validator, "invalidCount"), messagesAmount);
    }
}