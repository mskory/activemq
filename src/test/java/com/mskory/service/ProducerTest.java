package com.mskory.service;

import com.mskory.model.Pojo;
import com.mskory.model.PojoMessage;
import com.mskory.service.execute.ProducersExecutor;
import com.mskory.service.supply.RandomPojoSupplier;
import com.mskory.service.supply.SessionSupplier;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProducerTest {
    @Mock
    SessionSupplier sessionSupplier;
    @Mock
    Session session;
    @Mock
    Connection connection;
    @Mock
    Queue queue;
    @Mock
    MessageProducer msgProducer;
    @Mock
    ObjectMessage message;
    @Mock
    RandomPojoSupplier<Pojo> randomSupplier;
    @Mock
    PoisonPillProducer poisonPillProducer;
    Pojo pojo = new PojoMessage();
    int totalMessagesAmount = 10;
    String [] args = new String[]{String.valueOf(10)};
    Configuration config =  new Configuration(args);
    Producer producer;

    ResultsProcessor results = new ResultsProcessor();

    @BeforeEach
    void setUp() throws JMSException {
        producer = new Producer(config,  sessionSupplier, randomSupplier, results);
        when(randomSupplier.generate()).thenReturn(pojo);
        when(sessionSupplier.createConnection()).thenReturn(connection);
        when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenReturn(session);
      //  when(sessionSupplier.createSession()).thenReturn(session);
        when(session.createQueue(config.getQueueName())).thenReturn(queue);
        when(session.createProducer(queue)).thenReturn(msgProducer);
        when(session.createObjectMessage(pojo)).thenReturn(message);
    }

    @Test
    void producerClass_start_Ok(){
        producer.run();
        assertEquals(totalMessagesAmount, producer.getProcessedMessages());
        assertTrue(producer.start());
    }

    @Test
    void producerExecutor_start_Ok() throws JMSException {
//        when(session.createConsumer(queue)).thenReturn(msgConsumer);
//        when(msgConsumer.receive()).thenReturn(message);
//        when(message.getIntProperty("consumerCount")).thenReturn(consumersAmount);
        when(poisonPillProducer.send()).thenReturn(true);
        config.setProducersNumber(10);
//        PoisonPillProducer poisonPillProducer = new PoisonPillProducer(config.getQueueName(), sessionSupplier);
//        when(AdvisorySupport.getConsumerAdvisoryTopic(queue)).thenReturn(consumerAdvisoryTopic);
        ProducersExecutor executor= new ProducersExecutor(config, randomSupplier, sessionSupplier);
        executor.setPoisonPillProducer(poisonPillProducer);
        executor.start();
        Awaitility.await().until(() -> !executor.isAlive());
        assertEquals(10, executor.getProducersCount());
        verify(poisonPillProducer).send();
    }
}