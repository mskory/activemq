package com.mskory.service;

import com.mskory.model.Pojo;
import com.mskory.service.execute.ConsumersExecutor;
import com.mskory.service.supply.SessionSupplier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import java.util.concurrent.ConcurrentLinkedQueue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConsumerTest {
    @Mock
    SessionSupplier sessionSupplier;
    @Mock
    Session session;
    @Mock
    Queue queue;
    @Mock
    MessageConsumer msgConsumer;
    @Mock
    ObjectMessage objectMessage;
    @Mock
    TextMessage poisonPill;
    Pojo simplePojo = new Pojo();
    int messagesAmount = 10;
    String[] args = new String[]{String.valueOf(messagesAmount)};
    java.util.Queue<Pojo> messages = new ConcurrentLinkedQueue<>();
    Configuration config = new Configuration(args);

    ResultsProcessor results = new ResultsProcessor();

    @BeforeEach
    void setUp() throws JMSException {
        when(sessionSupplier.createSession()).thenReturn(session);
        when(session.createQueue(config.getQueueName())).thenReturn(queue);
        when(session.createConsumer(queue)).thenReturn(msgConsumer);
    }

    @Test
    void consumerClass_start_Ok() throws JMSException, InterruptedException {
        Consumer consumer = new Consumer(config, sessionSupplier, messages, results);
        lenient().when(msgConsumer.receive()).thenReturn(objectMessage);
        lenient().when(objectMessage.getObject()).thenReturn(simplePojo);
        consumer.start();
        Thread.sleep(100);
        lenient().when(poisonPill.getText()).thenReturn("STOP");
        lenient().when(msgConsumer.receive()).thenReturn(poisonPill);
        assertEquals(consumer.getMessages().size(), consumer.getMessagesCount());

    }

    @Test
    void consumerExecutorClass_start_Ok() throws JMSException {
        config.setConsumersNumber(3);
        ConsumersExecutor executor = new ConsumersExecutor(config, sessionSupplier, messages);
        when(poisonPill.getText()).thenReturn("STOP");
        when(msgConsumer.receive()).thenReturn(poisonPill);
        executor.run();
        assertEquals(3, executor.getConsumersCount());
    }
}