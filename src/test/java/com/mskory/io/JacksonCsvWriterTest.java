package com.mskory.io;

import com.mskory.model.Pojo;
import com.mskory.model.PojoMessage;
import com.mskory.service.PojoValidator;
import com.mskory.service.supply.RandomPojoSupplier;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
class JacksonCsvWriterTest {
    static RandomPojoSupplier<Pojo> randomPojoSupplier;
    static Queue<Pojo> validPojosQueue;
    static Queue<Pojo> invalidPojosQueue;
    static PojoValidator validator = new PojoValidator(Locale.ENGLISH, new LinkedList<>(), new AtomicBoolean());
    static int pojosNumber = 10;

    static JacksonCsvWriter writer;
    static PojoMessage simplePojo;

    @BeforeAll
    static void beforeAll() {
        simplePojo = new PojoMessage("Anabanana", 69, LocalDate.now());
        randomPojoSupplier = Mockito.mock(RandomPojoSupplier.class);
        when(randomPojoSupplier.generate()).thenReturn(simplePojo);
    }
    @BeforeEach
    void setUp() {
        validPojosQueue = Stream.generate(randomPojoSupplier::generate)
                .limit(pojosNumber)
                .collect(Collectors.toCollection(LinkedList::new));
        invalidPojosQueue = Stream.generate(randomPojoSupplier::generate)
                .limit(pojosNumber)
                .collect(Collectors.toCollection(LinkedList::new));
        ReflectionTestUtils.setField(validator, "valid", validPojosQueue);
        ReflectionTestUtils.setField(validator, "invalid", invalidPojosQueue);
        ReflectionTestUtils.setField(validator, "validatorState", new AtomicBoolean(false));
        writer = new JacksonCsvWriter(validator);
    }

    @Test
    void writeValid_Ok() {
        writer.setWriteValid(true);
        writer.write();
        assertEquals(0, validPojosQueue.size());
        assertEquals(10, writer.getMessagesWrote());
    }
    @Test
    void writeInvalid_Ok() {
        writer.setWriteInvalid(true);
        writer.write();
        assertEquals(0, invalidPojosQueue.size());
        assertEquals(10, writer.getMessagesWrote());
    }

    @Test
    void writeInvalidAndInvalid_Ok() {
        writer.setWriteInvalid(true);
        writer.setWriteValid(true);
        writer.write();
        assertEquals(0, validPojosQueue.size());
        assertEquals(0, invalidPojosQueue.size());
        assertEquals(20, writer.getMessagesWrote());
    }
}